# Fetch Jobs & Freelancers Details from [Freelancer.com](https://www.freelancer.com/)

Ready to use code script Flask web framework - python.

### About the script.

Easy available ready to use code to fetch jobs from freelancer.com.

Created in python.

Dependencies.
> Flask

> freelancersdk

## Configuration

You may feel free to edit the *jobs_config.json* as per your needs.



