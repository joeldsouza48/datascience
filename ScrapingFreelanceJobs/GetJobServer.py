from freelancersdk.session import Session
from freelancersdk.resources.projects.projects import search_projects
from freelancersdk.resources.projects.exceptions import ProjectsNotFoundException
from freelancersdk.resources.projects.helpers import (
    create_search_projects_filter,
    create_get_projects_user_details_object,
    create_get_projects_project_details_object,
)
from freelancersdk.resources.users.users import search_freelancers
from freelancersdk.resources.users.exceptions import \
    UsersNotFoundException
from freelancersdk.resources.users.helpers import (
    create_get_users_details_object
)

import json
from flask import jsonify 
from flask import Flask
from flask_cors import CORS, cross_origin

app = Flask(__name__)
cors = CORS(app)
app.config["CORS_HEADERS"] = "Content-Type"

# Using Global Variables

globalConfig = {}
with open('jobs_config.json') as json_data_file:
    globalConfig = json.load(json_data_file)
    

def getProjectsBySkills(queryString):
    if queryString == "":
        return {"data": {} , "message": "Query String Not Found"}

    # URL Staging or Production
    url = globalConfig['freelancerURL']
    
    # User API token
    oauthFreelancerToken = globalConfig['oauthFreelancerToken']
    
    session = Session(oauth_token=oauthFreelancerToken, url=url)
    #query = queryString
    search_filter = create_search_projects_filter(
        sort_field= 'time_updated',
        or_search_query= True,
    )
    AllProjects = []
    try:
        AllProjects = search_projects(
            session,
            query=queryString,
            limit=500,
            offset = 0,
            search_filter=search_filter
        )
    except:
        #return {"data": None , "message": str(e.message), "error": str(e.error_code) }
        print("Error")
        return {"data": None , "message": "Error Occured", "error": "Unable to Fetch Result" }
        
    else:
        return {"data": AllProjects , "message": "Okay", "error": "err"}
    
    return AllProjects

def getFreelancersBySkills(queryString):
    if queryString == "":
        return {"data": {} , "message": "Query String Not Found"}

    # URL Staging or Production
    url = globalConfig['freelancerURL']
    
    # User API token
    oauthFreelancerToken = globalConfig['oauthFreelancerToken']
    
    session = Session(oauth_token=oauthFreelancerToken, url=url)
    user_details = create_get_users_details_object(
        cover_image=False,
        reputation=True,
        display_info=True
    )
    try:
        result = search_freelancers(
            session,
            query= queryString,
            user_details=user_details,
        )
    except UsersNotFoundException as e:
        print('Error message: {}'.format(e))
        return None
    else:
        return result


@app.route("/GetJobsBySkillFreelancer/<jobSkill>")
@cross_origin()
def getJobsBySkill(jobSkill):
    if len(jobSkill) < 0 :
        return jsonify({"message":"", "error":"Unable to Process Request"})    
    
    data = {'source':'Freelancer'}
    #print(jobSkill)

    # Fetch Project available
    allProjects = getProjectsBySkills(jobSkill)
    #print("Total Projects Available :: {}".format('{:,.0f}'.format(allProjects["data"]["total_count"])))
    
    data['totalProjectAvailable'] = '{:,.0f}'.format(allProjects["data"]["total_count"])
    totalBidValue = 0
    for singleProject in allProjects["data"]["projects"]:
        try:
            totalBidValue = totalBidValue + singleProject["bid_stats"]["bid_avg"] * singleProject["currency"]["exchange_rate"]
        except:
            pass
    #print("Total Projects Avg bid :: INR {}".format( '{:,.2f}'.format(totalBidValue * globalConfig['exchangeCurrency'] / len(allProjects["data"]["projects"])) ))

    data["avgBidAmount"] = str('INR. {:,.2f}'.format(totalBidValue * globalConfig['exchangeCurrency'] / len(allProjects["data"]["projects"])))

    # Fetch Freelancer available
    #print("Total Freelancer Available :: {}".format('{:,.0f}'.format(getFreelancersBySkills(jobSkill)["total_count"])))
    
    data["noOfFreelancers"] = str('{:,.0f}'.format(getFreelancersBySkills(jobSkill)["total_count"]))

    return jsonify({"message":"Success", "error":"", "data":data})

if __name__ == "__main__":
    app.run(port=4986)